//
//  CanPosition.h
//  BeerCanBully_Objective-C
//
//  Created by roctian on 2016/10/24.
//  Copyright © 2016年 roctian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CanPosition : NSObject

@property(nonatomic) float x;
@property(nonatomic) float y;
@property(nonatomic) float z;

@end
