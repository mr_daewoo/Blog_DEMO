//
//  CAAnimation+CAAnimation_SceneName.h
//  Badger_Objective-C
//
//  Created by roctian on 2016/10/27.
//  Copyright © 2016年 roctian. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CAAnimation (SceneName)

+(CAAnimation *)animationWithSceneName:(NSString *)name;

@end
