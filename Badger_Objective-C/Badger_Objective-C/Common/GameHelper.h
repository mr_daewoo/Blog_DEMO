//
//  GameHelper.h
//  Badger_Objective-C
//
//  Created by roctian on 2016/10/28.
//  Copyright © 2016年 roctian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameHelper : NSObject

+(float)TwoPositionDistance:(SCNVector3)one two:(SCNVector3)two;
@end
